FROM gradle:6.3.0-jdk14

WORKDIR /home/gradle/src
COPY --chown=gradle:gradle . /home/gradle/src

RUN chmod +x gradlew

CMD ./gradlew appRun --stacktrace