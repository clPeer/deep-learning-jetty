package org.gradle;

import java.io.*;
import java.util.Map;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hex.genmodel.easy.prediction.MultinomialModelPrediction;
import hex.genmodel.easy.*;

@WebServlet(name = "PredictServlet", urlPatterns = { "gbm" }, loadOnStartup = 1)
public class PredictServlet extends HttpServlet {
    static EasyPredictModelWrapper dlModel;

    static {
        deeplearning_d5c35043_8929_441a_9a23_dc44b06b519f rawDL = new deeplearning_d5c35043_8929_441a_9a23_dc44b06b519f();
        dlModel = new EasyPredictModelWrapper(rawDL);
    }

    @SuppressWarnings("unchecked")
    private void fillRowDataFromHttpRequest(HttpServletRequest request, RowData row) {
        Map<String, String[]> parameterMap;
        parameterMap = request.getParameterMap();
        for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
            String key = entry.getKey();
            String[] values = entry.getValue();
            for (String value : values) {
                if (value.length() > 0) {
                    row.put(key, value);
                }
            }
        }
    }

    private MultinomialModelPrediction predictDeepLearning(RowData row) throws Exception {
        return dlModel.predictMultinomial(row);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RowData row = new RowData();
        fillRowDataFromHttpRequest(request, row);
        String responseString = "Class probabilities:\n";
        try {
            MultinomialModelPrediction p = predictDeepLearning(row);
            for (int i = 0; i < p.classProbabilities.length; i++) {
                if(i < p.classProbabilities.length - 1) {
                    responseString += p.classProbabilities[i] + ",";
                }
                else {
                    responseString += p.classProbabilities[i] + "\n\n";
                }
            }
            responseString += "label: " + p.label + "\n";
            response.getWriter().print(responseString);
        }
        catch (Exception e) {
            // Prediction failed.
            response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, e.getMessage());
        }
    }

}